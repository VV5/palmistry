# Changelog

## v1.2.0
- Refactor video modal
- Added a function to open Palmistry Video at any time
- Upgraded `vue-loader` to v15

## v1.1.0
- Fixed link from Resource 2 into existing framework
- Merged Resource 2 with existing framework 
- Added button to restart
- Added missing poem for certain components
- Removed colour for active words when mouse over
- Fixed path for images and fonts

## v1.0.0
Initial release

- Upgraded Font Awesome to v5
- Added remaining components to complete the skeletal framework
- Added Milonga fonts
- Fixed site not opening locally via `index.html`
- Optimised webpack configuration for production
