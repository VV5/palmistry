import Vue from 'vue'
import Router from 'vue-router'
import MediaContent from '@/components/MediaContent'
import Introduction from '@/components/Introduction'
import Clue from '@/components/Clue'
import Inference from '@/components/Inference'
import Diction from '@/components/Diction'
import Imagery from '@/components/Imagery'
import ConnectDots from '@/components/ConnectDots'
import SummaryView from '@/components/SummaryView'

Vue.use(Router)

export default new Router({
  base: window.location.pathname,
  routes: [
    {
      path: '/',
      name: 'MediaContent',
      component: MediaContent,
      meta: {
        title: 'Media Content'
      }
    },
    {
      path: '/introduction',
      name: 'Introduction',
      component: Introduction,
      meta: {
        title: 'Introduction'
      }
    },
    {
      path: '/clue',
      name: 'Clue',
      component: Clue,
      meta: {
        title: 'Clue'
      }
    },
    {
      path: '/inference',
      name: 'Inference',
      component: Inference,
      meta: {
        title: 'Inference'
      }
    },
    {
      path: '/diction',
      name: 'Diction',
      component: Diction,
      meta: {
        title: 'Diction'
      }
    },
    {
      path: '/imagery',
      name: 'Imagery',
      component: Imagery,
      meta: {
        title: 'Imagery'
      }
    },
    {
      path: '/connect-dots',
      name: 'ConnectDots',
      component: ConnectDots,
      meta: {
        title: 'Connect the Dots'
      }
    },
    {
      path: '/summary',
      name: 'Summary',
      component: SummaryView,
      meta: {
        title: 'Summary'
      }
    }
  ]
})
