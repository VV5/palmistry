import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state: {
    correctScreen: false,
    showAnalyse: false
  },
  mutations: {
    correctScreen (state, status) {
      state.correctScreen = status
    },
    isShowAnalyse (state) {
      state.showAnalyse = true
    }
  }
})
