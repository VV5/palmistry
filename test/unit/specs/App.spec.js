import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import App from '@/App'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('App', () => {
  let wrapper, store, state, mutations

  beforeEach(() => {
    state = {}

    mutations = {
      correctScreen: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    wrapper = shallowMount(App, {
      attachToDocument: true,
      store,
      localVue,
      computed: {
        correctScreen: () => true
      }
    })
  })

  test('should trigger resize event correctly', () => {
    window.innerWidth = 1080
    window.innerHeight = 1920

    wrapper.trigger('resize')

    expect(mutations.correctScreen).toHaveBeenCalled()
  })
})
