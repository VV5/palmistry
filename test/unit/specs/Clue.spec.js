import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Clue from '@/components/Clue'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Clue', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Clue, {
      attachToDocument: true
    })
  })

  test('should highlight the word when hover over', () => {
    wrapper.setData({ instructionIndex: 1 })

    wrapper.findAll('.active-word').at(0).trigger('mouseover')

    expect(wrapper.findAll('.active-word').at(0).attributes().style).toBeTruthy()
  })

  test('should set highlighted text as selected when clicked', () => {
    wrapper.setData({ instructionIndex: 1 })

    wrapper.findAll('.active-word').at(0).trigger('click')

    expect(wrapper.vm.poem[0].selected).toBe(true)
  })

  test('should navigate to inference if button is clicked', () => {
    const $route = {
      path: '/inference'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Clue, {
      router,
      localVue,
      computed: {
        showNextButton: () => true
      }
    })

    const count = wrapper.vm.instructions.length - 1

    wrapper.setData({ instructionIndex: count })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })

  test('should increment instructionIndex when button is clicked', () => {
    expect(wrapper.vm.instructionIndex).toBe(0)

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.instructionIndex).toBe(1)
  })
})
