import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import ConnectDots from '@/components/ConnectDots'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('ConnectDots', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(ConnectDots)
  })

  test('should increment instructionIndex when clicked', () => {
    expect(wrapper.vm.instructionIndex).toBe(0)

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.instructionIndex).toBe(1)
  })

  test('should navigate to summary page correctly', () => {
    const $route = {
      path: '/summary'
    }

    const router = new VueRouter()

    wrapper = shallowMount(ConnectDots, {
      router, localVue
    })

    wrapper.setData({
      instructionIndex: wrapper.vm.instructions.length - 1
    })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
