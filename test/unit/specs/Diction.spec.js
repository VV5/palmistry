import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Diction from '@/components/Diction'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Diction', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Diction)
  })

  test('should show cursor as pointer when mouseover any buttons', () => {
    wrapper.findAll('.active-word').at(0).trigger('mouseover')
  })

  test('should set selected to true when each active word is clicked', () => {
    expect(wrapper.vm.poem[0].selected).toBe(false)

    wrapper.findAll('.active-word').at(0).trigger('click')

    expect(wrapper.vm.poem[0].selected).toBe(true)
  })

  test('should increment instructionIndex when button is clicked', () => {
    wrapper = shallowMount(Diction, {
      computed: {
        showNextButton: () => true
      }
    })

    expect(wrapper.vm.instructionIndex).toBe(0)

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.instructionIndex).toBe(1)
  })

  test('should navigate to Connect Dots correctly', () => {
    const $route = {
      path: '/imagery'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Diction, {
      router,
      localVue,
      computed: {
        showNextButton: () => true
      }
    })

    wrapper.setData({
      instructionIndex: wrapper.vm.instructions.length - 1
    })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
