import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Imagery from '@/components/Imagery'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Imagery', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Imagery, {
      attachToDocument: true
    })
  })

  test('should set selectedItem with the correct item when image is clicked', () => {
    wrapper.find('.show-image').trigger('click', {
      item: wrapper.vm.poem[0]
    })

    expect(wrapper.vm.selectedItem).toBe(wrapper.vm.poem[0])
  })

  test('should navigate to ConnectDots when button is clicked', () => {
    const $route = {
      path: '/connect-dots'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Imagery, {
      attachToDocument: true,
      router,
      localVue
    })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
