import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Inference from '@/components/Inference'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Inference', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Inference)
  })

  test('should open modal when that particular inference is clicked', () => {
    wrapper.findAll('.is-inference').at(0).trigger('click')

    expect(wrapper.vm.selectedInference).toBe(wrapper.vm.inferences[0])
    expect(wrapper.vm.isActive).toBe(true)
  })

  test('should navigate to diction correctly when button is clicked', () => {
    const $route = {
      path: '/diction'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Inference, {
      router,
      localVue,
      computed: {
        showNextButton: () => true
      }
    })

    wrapper.find('.goto-diction').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
