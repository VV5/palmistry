import { shallowMount, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Introduction from '@/components/Introduction'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Introduction', () => {
  let wrapper, store, state, mutations

  beforeEach(() => {
    state = {}

    mutations = {
      isShowAnalyse: jest.fn()
    }

    store = new Vuex.Store({
      state, mutations
    })

    wrapper = shallowMount(Introduction, {
      store, localVue
    })
  })

  test('should show analyse when button is clicked', () => {
    wrapper.find('.is-first-button').trigger('click')

    expect(mutations.isShowAnalyse).toHaveBeenCalled()
  })

  test('should navigate to clue correctly when button is clicked', () => {
    const $route = {
      path: '/clue'
    }

    const router = new VueRouter()

    wrapper = shallowMount(Introduction, {
      store,
      router,
      localVue,
      computed: {
        showAnalyse: () => true
      }
    })

    wrapper.find('.is-start').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
