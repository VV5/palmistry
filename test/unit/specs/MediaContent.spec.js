import { shallowMount } from '@vue/test-utils'
import MediaContent from '@/components/MediaContent'
import MediaModal from '@/components/VideoModal'

describe('MediaContent', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(MediaContent, {
      attachToDocument: true
    })

    window.HTMLMediaElement.prototype.load = () => {}
    window.HTMLMediaElement.prototype.pause = () => {}
    window.HTMLMediaElement.prototype.play = () => {}
  })

  test('should set isPlaying to true when audio is being played', () => {
    wrapper.find('.play-button').trigger('click')

    wrapper.vm.playAsset('audio')

    expect(wrapper.vm.isPlaying).toBe(true)
  })

  test('should open modal when attempted to play video', () => {
    wrapper.setData({ contentIndex: 1 })

    wrapper.find('.play-button').trigger('click')

    wrapper.vm.playAsset('video')

    expect(wrapper.vm.isActive).toBe(true)
  })

  test('should show next button when poem has ended', () => {
    wrapper.find('audio').trigger('ended')

    expect(wrapper.vm.isPlaying).toBe(false)
  })

  test('should close the modal correctly', () => {
    wrapper.setData({ isActive: true })

    wrapper.find(MediaModal).vm.$emit('closeModal')

    expect(wrapper.vm.isActive).toBe(false)
  })

  test('should increment contentIndex when button is clicked', () => {
    expect(wrapper.vm.contentIndex).toBe(0)

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.contentIndex).toBe(1)
  })
})
