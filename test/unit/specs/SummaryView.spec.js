import { shallowMount, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import SummaryView from '@/components/SummaryView'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('SummaryView', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(SummaryView)
  })

  test('should restart to the first page correctly when button is clicked', () => {
    const $route = {
      path: '/'
    }

    const router = new VueRouter()

    wrapper = shallowMount(SummaryView, {
      router, localVue
    })

    window.location.reload = () => {}

    wrapper.find('.restart-button').trigger('click')

    expect(wrapper.vm.$route.path).toBe($route.path)
  })
})
