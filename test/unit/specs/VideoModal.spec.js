import { shallowMount } from '@vue/test-utils'
import VideoModal from '@/components/VideoModal'

describe('VideoModal', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(VideoModal, {
      attachToDocument: true,
      propsData: {
        content: {
          asset: {
            path: require(`@/assets/video/palmistry-poem.mp4`)
          }
        },
        isActive: true
      }
    })

    window.HTMLMediaElement.prototype.pause = () => {}
  })

  test('should close modal when ESC button is pressed', () => {
    wrapper.trigger('keyup', { key: 'Escape' })
    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })

  test('should not do anything when buttons other than ESC is pressed', () => {
    wrapper.trigger('keyup')

    expect(wrapper.emitted().closeModal).toBeFalsy()
  })

  test('should close modal when video has finished playing', () => {
    wrapper.find('video').trigger('ended')
    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })

  test('should close modal when modal background is clicked', () => {
    wrapper.find('.modal-background').trigger('click')
    wrapper.vm.$emit('closeModal')

    expect(wrapper.emitted().closeModal).toBeTruthy()
  })
})
